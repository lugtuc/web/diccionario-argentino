function loadJSON(file, callback) {   
  var xobj = new XMLHttpRequest();
      xobj.overrideMimeType("application/json");
  xobj.open('GET', file, true); // Replace 'appDataServices' with the path to your file
  xobj.onreadystatechange = function () {
        if (xobj.readyState == 4 && xobj.status == "200") {
          // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
          callback(xobj.responseText);
        }
  };
  xobj.send(null);
}

function getParams(url) {
	var params = {};
	var parser = document.createElement('a');
	parser.href = url;
	var query = parser.search.substring(1);
	var vars = query.split('&');
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split('=');
		params[pair[0]] = decodeURIComponent(pair[1]);
	}
	return params;
}

function paginar(lista, cantidad, numero) {
  return lista.slice((numero - 1) * cantidad, numero * cantidad);
}

function eliminarDiacriticos(texto) {
  return texto.normalize('NFD').replace(/[\u0300-\u036f]/g,"").toLowerCase()
}

function buscar (diccionario) {
  var seleccion = diccionario[selector.value]
  var coleccion = []
  if (selector.value == 'A - Z') {
    seleccion = {}
    for (const categoria in diccionario) {
      // console.log('union categorias: ', categoria)
      coleccion = [...coleccion, ...Object.keys(diccionario[categoria])]
      seleccion = { ...seleccion, ...diccionario[categoria] }
    }
  } else {
    coleccion = Object.keys(seleccion)
  }
  var buscador = document.getElementById('searchBox')
  // console.log('seleccion', seleccion)
  var lista = document.getElementById('lista')
  var busqueda = buscador.value
  // console.log('busqueda: ', busqueda, eliminarDiacriticos(busqueda))
  var ordenadas = coleccion.sort(function (a, b) {
    if (a > b) {
      return 1
    } else {
      if (a == b) {
        return 0
      } else {
        return -1
      }
    }
  })
  var palabras = ''
  var cantPalPag = parseInt(document.getElementById('cantPalPag').value, 10)
  if (selector.value != 'A - Z' || busqueda) {
    ordenadas = ordenadas.filter((pal) => eliminarDiacriticos(pal).includes(eliminarDiacriticos(busqueda)))
    // console.log('max: ', ordenadas.length / cantPalPag, ordenadas)
    document.getElementById('pagActual').max = Math.ceil(ordenadas.length / cantPalPag)
    if (document.getElementById('pagActual').max < document.getElementById('pagActual').value) {
      document.getElementById('pagActual').value = 1
    }
    if (document.getElementById('pagActual').min > document.getElementById('pagActual').value) {
      document.getElementById('pagActual').value = document.getElementById('pagActual').max
    }
    document.getElementById('pagAct').innerHTML = 'Pág ' + document.getElementById('pagActual').value
    paginar(ordenadas, cantPalPag, document.getElementById('pagActual').value).forEach(function (palabra) {
      palabras = palabras + `
        <li class="definicion">
          <b class="palabra">${palabra}:</b> ${seleccion[palabra]}
        </li>
      `
    })
    lista.innerHTML = palabras
    document.getElementById('tooltip').classList.add('d-none')
    if (!palabras) {
      document.getElementById('paginador').classList.add('d-none')
      document.getElementById('nada').classList.remove('d-none')
    } else {
      document.getElementById('nada').classList.add('d-none')
      document.getElementById('paginador').classList.remove('d-none')
    }
  }
  if (selector.value == 'A - Z' && !busqueda) {
    lista.innerHTML = ''
    document.getElementById('tooltip').classList.remove('d-none')
    document.getElementById('nada').classList.add('d-none')
  }

}

window.onload = function () {
  document.getElementById('nada').classList.add('d-none')
  loadJSON('diccionario.json', function(response) {
  // Parsing JSON string into object
    var diccionario = JSON.parse(response)
    diccionario['A - Z'] = {}
    // console.log('diccionario', diccionario)
    var selector = document.getElementById('selector')
    var opciones = ''
    Object.keys(diccionario).forEach(function (letra) {
      opciones = opciones + `
      <option value="${letra}" ${ letra == 'A - Z' ? 'selected': '' }>
          ${letra}
      </option>
    `
    })
    selector.innerHTML = opciones
    selector.addEventListener("change", function () {
      buscar(diccionario)
    })

    document.getElementById('searchBox').addEventListener('keypress', function (e) {
      if ( e.key == 'Enter') {
        buscar(diccionario)
      }
    })

    var boton = document.getElementById('buscar')
    boton.addEventListener("click", function () {
      buscar(diccionario)
    })
    
    var params = getParams(window.location.href)
    // console.log('params', params)
    if (params.palabra) {
      selector.value = params.categoria
      document.getElementById('searchBox').value = params.palabra
    }

    document.getElementById('pagSig').addEventListener('click', function() {
      document.getElementById('pagActual').value = parseInt(document.getElementById('pagActual').value, 10) + 1
      buscar(diccionario)
    })

    document.getElementById('pagAnt').addEventListener('click', function() {
      document.getElementById('pagActual').value = parseInt(document.getElementById('pagActual').value, 10) - 1
      buscar(diccionario)
    })

    document.getElementById('cantPalPag').addEventListener('change', function() {
      buscar(diccionario)
    })

    document.getElementById('pagActual').addEventListener("change", function () {
      buscar(diccionario)
    })

    if (selector.value != 'A - Z' || document.getElementById('searchBox').value){
      buscar(diccionario)
    } else {
      document.getElementById('pagActual').classList.add('d-none')
    }
 });
}
// var diccionario = document.getElementById('data').innerHTML
// var mydata = JSON.parse(diccionario);
// console.log('diccionario', diccionario);
