# Diccionario
Página estática que muestra palabras y sus significados.

Las palabras se encuentran en el archivo diccionario.json agrupadas por categorias (letra de inicio u otra forma de clasificación). El orden de las palabras dentro de cada categoría es indistinto, debido a que al mostrarse se ordenan automáticamente.

## Licencia
Distribuido bajo GPL versión 3 o superior
